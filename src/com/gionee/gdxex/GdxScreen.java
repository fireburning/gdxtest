package com.gionee.gdxex;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.math.Matrix4;
import com.gionee.gdxex.Texture.TextureFilter;

public class GdxScreen implements Screen {
    /** the SpriteBatch used to draw the background, logo and text **/
    private SpriteBatch spriteBatch;
    /** the background texture **/
    private final Texture background;
    /** the logo texture **/
    private final Texture logo;
    /** is done flag **/
    private boolean isDone = false;
    /** view & transform matrix **/
    private final Matrix4 viewMatrix = new Matrix4();
    private final Matrix4 transformMatrix = new Matrix4();
    private final Texture robot;
    
    public GdxScreen (Context context) {
        GLCanvas_New();
        Drawable dr = context.getResources().getDrawable(R.drawable.planet);
        TextureData td = new DrawableTextureData(CDrawable.createCDrawable(dr), true);
        
        background = new Texture(td);
        background.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        Drawable titleDr = context.getResources().getDrawable(R.drawable.title);
        TextureData titleTd = new DrawableTextureData(CDrawable.createCDrawable(titleDr), true);
        
        logo = new Texture(titleTd);
        logo.setFilter(TextureFilter.Linear, TextureFilter.Linear);

        Drawable dr_robot = context.getResources().getDrawable(R.drawable.robot);
        TextureData td_robot = new DrawableTextureData(CDrawable.createCDrawable(dr_robot), true);
        robot = new Texture(td_robot);
    }

    private void GLCanvas_New() {
        spriteBatch = new SpriteBatch();
        viewMatrix.setToOrtho2D(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        spriteBatch.setProjectionMatrix(viewMatrix);
        spriteBatch.enableBlending();
    }

    public boolean isDone () {
        return isDone;
    }

    public void update (float delta) {
//        if (Gdx.input.justTouched()) {
//            isDone = true;
//        }
    }

    public void draw (float delta) {
        GLCanvas_clear();

        GLCanvas_setMatrix();
        GLCanvas_begin();
        
//        spriteBatch.disableBlending();
//        spriteBatch.setColor(Color.WHITE);
        GLCanvas_draw(background, new Rect(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
//        spriteBatch.enableBlending();
        
        GLCanvas_draw(logo, new Rect(0, 0, 512, 256), new Rect(0, 320 - 128, 480, 128));
        GLCanvas_draw(robot, new Rect(0, 0, robot.getWidth(), robot.getHeight()), new Rect(0, 0, robot.getWidth(), robot.getHeight()));
//        spriteBatch.setBlendFunction(GL10.GL_ONE, GL10.GL_ONE_MINUS_SRC_ALPHA);
        
        GLCanvas_end();
    }

    private void GLCanvas_draw(Texture tex, Rect srcRect, Rect dstRect) {
        spriteBatch.draw(tex, dstRect.left, dstRect.top, dstRect.right, dstRect.bottom, srcRect.left, srcRect.top, srcRect.right, srcRect.bottom, false, false);
    }

    private void GLCanvas_draw(Texture tex, Rect dstRect) {
        spriteBatch.draw(tex, dstRect.left, dstRect.top, dstRect.right, dstRect.bottom, 0, 0, tex.getWidth(), tex.getHeight(), false, false);
    }
    
    private void GLCanvas_end() {
        spriteBatch.end();
    }

    private void GLCanvas_begin() {
        spriteBatch.begin();
    }

    private void GLCanvas_setMatrix() {
        spriteBatch.setTransformMatrix(transformMatrix);
    }

    private void GLCanvas_clear() {
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void render (float delta) {
        update(delta);
        draw(delta);
    }

    @Override
    public void resize (int width, int height) {
    }

    @Override
    public void show () {
    }

    @Override
    public void hide () {
    }

    @Override
    public void pause () {
    }

    @Override
    public void resume () {
    }

    @Override
    public void dispose () {
        spriteBatch.dispose();
        background.dispose();
        logo.dispose();
    }
}
