package com.gionee.gdxex;

import android.opengl.GLES20;
import android.util.Log;

public class Debug {
	public final static String MAY_ERR = "May_Error";
	private static String[] sLogInfo = new String[2];
	private final static String mMPrefix = "mengqj";
	private final static String mTPrefix = "gaojt";
	private final static String mUPrefix = "gaols";
	private final static String mFPrefix = "jipf";
    private static final String TAG = "GLES-DEBUG";

	private Debug() {
	}

	public static void logInfo(String[] logInfo, String prefix, String desc) {
		final StringBuffer sbPrefix = new StringBuffer(prefix);

		StackTraceElement[] stacks = Thread.currentThread().getStackTrace();
		String fileName = stacks[4].getFileName();
		logInfo[0] = sbPrefix.append('-')
				.append(fileName.substring(0, fileName.indexOf('.')))
				.toString();
		Log.d("log", "logInfo[0]=" + logInfo[0]);
		final StringBuffer sbDesc = new StringBuffer();
		logInfo[1] = sbDesc.append("")
				.append(stacks[4].getMethodName()).append("::")
				.append(stacks[4].getLineNumber()).append("::")
				.append(desc).toString();
	}

	public static void logInfoWithTag(String[] logInfo, String prefix, String tag, String desc) {
		final StringBuffer sbPrefix = new StringBuffer(prefix);

		StackTraceElement[] stacks = Thread.currentThread().getStackTrace();
//		String fileName = stacks[4].getFileName();
		logInfo[0] = sbPrefix.append('-')
				.append(tag)
				.toString();
		Log.d("log", "logInfo[0]=" + logInfo[0]);
		final StringBuffer sbDesc = new StringBuffer();
		logInfo[1] = sbDesc.append("")
				.append(stacks[4].getMethodName()).append("::")
				.append(stacks[4].getLineNumber()).append("::")
				.append(desc).toString();
	}
	
	public static void d(String tag, String desc) {
		Log.d(tag, desc);
	}
	
	public static void f(String tag, String desc) {
		logInfoWithTag(sLogInfo, mFPrefix, tag, desc);
		Log.d(sLogInfo[0], sLogInfo[1]);
	}

	public static void m(String tag, String desc) {
		logInfoWithTag(sLogInfo, mMPrefix, tag, desc);
		Log.d(sLogInfo[0], sLogInfo[1]);
	}

	public static void t(String tag, String desc) {
		logInfoWithTag(sLogInfo, mTPrefix, tag, desc);
		Log.d(sLogInfo[0], sLogInfo[1]);
	}

	public static void u(String tag, String desc) {
		logInfoWithTag(sLogInfo, mUPrefix, tag, desc);
		Log.d(sLogInfo[0], sLogInfo[1]);
	}

	public static void f(String desc) {
		logInfo(sLogInfo, mFPrefix, desc);
		Log.d(sLogInfo[0], sLogInfo[1]);
	}

	public static void m(String desc) {
		logInfo(sLogInfo, mMPrefix, desc);
		Log.d(sLogInfo[0], sLogInfo[1]);
	}

	public static void t(String desc) {
		logInfo(sLogInfo, mTPrefix, desc);
		Log.d(sLogInfo[0], sLogInfo[1]);
	}

	public static void u(String desc) {
		logInfo(sLogInfo, mUPrefix, desc);
		Log.d(sLogInfo[0], sLogInfo[1]);
	}
	
	public static void mw(String tag, String desc, Throwable th) {
		logInfoWithTag(sLogInfo, mMPrefix, tag, desc);
		Log.w(sLogInfo[0], sLogInfo[1], th);
	}

	public static void tw(String tag, String desc, Throwable th) {
		logInfoWithTag(sLogInfo, mTPrefix, tag, desc);
		Log.w(sLogInfo[0], sLogInfo[1], th);
	}

	public static void uw(String tag, String desc, Throwable th) {
		logInfoWithTag(sLogInfo, mUPrefix, tag, desc);
		Log.w(sLogInfo[0], sLogInfo[1], th);
	}

	public static void fw(String tag, String desc, Throwable th) {
		logInfoWithTag(sLogInfo, mFPrefix, tag, desc);
		Log.w(sLogInfo[0], sLogInfo[1], th);
	}


	public static void mw(String desc, Throwable th) {
		logInfo(sLogInfo, mMPrefix, desc);
		Log.w(sLogInfo[0], sLogInfo[1], th);
	}

	public static void tw(String desc, Throwable th) {
		logInfo(sLogInfo, mTPrefix, desc);
		Log.w(sLogInfo[0], sLogInfo[1], th);
	}

	public static void uw(String desc, Throwable th) {
		logInfo(sLogInfo, mUPrefix, desc);
		Log.w(sLogInfo[0], sLogInfo[1], th);
	}

	public static void fw(String desc, Throwable th) {
		logInfo(sLogInfo, mFPrefix, desc);
		Log.w(sLogInfo[0], sLogInfo[1], th);
	}
	
	public static void asserts(boolean condition, String s) {
		if (!condition) {
		    throw new RuntimeException("Assertion failed! :" + s);
		}
	}

	public static void asserts(boolean condition) {
		if (!condition) {
		    throw new RuntimeException();
		}
	}
	
    public static void checkGlError(String op) {
        int error;
        while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
            Log.e(TAG , op + ": glError " + error);
            throw new RuntimeException(op + ": glError " + error);
        }
    }
}
