package com.gionee.gdxex;

import android.graphics.Bitmap;
import android.util.Log;

import com.badlogic.gdx.utils.GdxRuntimeException;
import com.gionee.gdxex.Pixmap.Format;

public class DrawableTextureData implements TextureData {
    private static final String TAG = "DrawableTextureData";
    boolean mbPrepared;
    boolean mbUseMipmap;
    Bitmap mBitmap;
    private Pixmap mPixmap;
    private int mWidth;
    private int mHeight;
    private Format mFormat;
    
    public DrawableTextureData(CDrawable dr) {
        this(dr, false);
    }
    
    public DrawableTextureData(CDrawable dr, boolean bMipmap) {
        mBitmap = dr.mBitmap;
        Log.d(TAG, "width = "+mBitmap.getWidth()+";height = "+mBitmap.getHeight());
        mbUseMipmap = bMipmap;
    }

    @Override
    public TextureDataType getType() {
        return TextureDataType.Pixmap;
    }

    @Override
    public boolean isPrepared() {
        return mbPrepared;
    }

    @Override
    public void prepare() {
        if (mbPrepared) throw new GdxRuntimeException("Already prepared");
        if (mPixmap == null) {
            mPixmap = new Pixmap(mBitmap);
            mWidth = mPixmap.getWidth();
            mHeight = mPixmap.getHeight();
            if (mFormat == null) mFormat = mPixmap.getFormat();
        }
        mbPrepared = true;
    }

    @Override
    public Pixmap consumePixmap() {
        if (!mbPrepared) throw new GdxRuntimeException("Call prepare() before calling getPixmap()");
        mbPrepared = false;
        Pixmap pixmap = mPixmap;
        mPixmap = null;
        return pixmap;
    }

    @Override
    public boolean disposePixmap() {
        return true;
    }

    @Override
    public void consumeCompressedData() {
        throw new GdxRuntimeException("This TextureData implementation does not upload data itself");
    }

    @Override
    public int getWidth() {
        return mWidth;
    }

    @Override
    public int getHeight() {
        return mHeight;
    }

    @Override
    public Format getFormat() {
        return mFormat;
    }

    @Override
    public boolean useMipMaps() {
         return mbUseMipmap;
    }

    @Override
    public boolean isManaged() {
        return true;
    }

}
