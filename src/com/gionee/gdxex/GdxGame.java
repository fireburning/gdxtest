package com.gionee.gdxex;

import android.content.Context;

public class GdxGame extends Game {

    /** Music needs to be a class property to prevent being disposed. */
    private FPSLogger fps;
    private Context mContext;
    
    public GdxGame(Context c) {
        mContext = c;
    }
    
    @Override
    public void render () {
        GdxScreen currentScreen = getScreen();
        
        // update the screen
        currentScreen.render(Gdx.graphics.getDeltaTime());

        // When the screen is done we change to the
        // next screen. Ideally the screen transitions are handled
        // in the screen itself or in a proper state machine.
        if (currentScreen.isDone()) {
            // dispose the resources of the current screen
            currentScreen.dispose();

            // if the current screen is a main menu screen we switch to
            // the game loop
            setScreen(new GdxScreen(mContext));
        }
        
        fps.log();
    }

    @Override
    public void create () {
        setScreen(new GdxScreen(mContext));
//        Gdx.input.setInputProcessor(new InputAdapter() {
//            @Override
//            public boolean keyUp (int keycode) {
//                if (keycode == Keys.ENTER && Gdx.app.getType() == ApplicationType.WebGL) {
//                    if (!Gdx.graphics.isFullscreen()) Gdx.graphics.setDisplayMode(Gdx.graphics.getDisplayModes()[0]);
//                }
//                return true;
//            }
//        });
        
        fps = new FPSLogger();
    }

    /** For this game each of our screens is an instance of InvadersScreen.
     * @return the currently active {@link InvadersScreen}. */
    @Override
    public GdxScreen getScreen () {
        return (GdxScreen)super.getScreen();
    }

}
