package com.gionee.gdxex;

import android.util.Log;

public class GnLog {
	private static final boolean DEBUG = true;
	private static final String TAG = "gioneelauncher";

	public static void d(String tag, String msg) {
		if (DEBUG) {
			Log.d(tag, msg);
			Log.d(TAG, "TAG="+tag+":"+msg);
		}
	}
	public static void w(String tag, String msg) {
		if (DEBUG) {
			Log.w(tag, msg);
			Log.d(TAG, "TAG="+tag+":"+msg);
		}
	}
}
