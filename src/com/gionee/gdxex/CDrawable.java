package com.gionee.gdxex;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.util.Log;


public class CDrawable extends Drawable {
	protected Bitmap mBitmap;
	private int mLeft;
	private int mTop;
	private boolean mIsMutable;
	private boolean mInvalidate;
	private boolean mBoundsChange;
	private Validator mValidator;
	private Rect mDstRect;
	private UpdateCallback mCb = null;
	private Paint mPaint = null;

	public static interface UpdateCallback {
		/**
		 * Called when the drawable needs to be redrawn. A view at this point
		 * should invalidate itself (or at least the part of itself where the
		 * drawable appears).
		 * 
		 * @param who
		 *            The drawable that is requesting the update.
		 */
		public void invalidateDrawable(Drawable who);
	};

	public int debugId = 0;

	public void setPaint(Paint paint) {
		mPaint = paint;
	}

	public void setUpdateCallback(UpdateCallback cb) {
		mCb = cb;
	}

	public static interface Validator {
		void draw(Bitmap bitmap);
	}

	public void setValidator(Validator validator) {
		mValidator = validator;
	}

	public Validator getValidator() {
		return mValidator;
	}

	public void invalidateSelf() {
		mInvalidate = true;
		if (mCb != null) {
			mCb.invalidateDrawable(this);
		}
	}

	public void validate() {
		if (mInvalidate && mValidator != null) {
			mValidator.draw(mBitmap);
			mInvalidate = false;
		}
	}

	public void updateSelf() {
		validate();
	}

	protected CDrawable() {
	}

	public CDrawable(Bitmap mIcon) {
		mBitmap = mIcon;
		mIsMutable = false;
	}

	public CDrawable(BitmapDrawable drawable) {
		Bitmap bitmap = drawable.getBitmap();
		mBitmap = bitmap;
		mIsMutable = false;
	}

	public CDrawable(CDrawable other) {
		this.mBitmap = other.mBitmap;
		this.mInvalidate = other.mInvalidate;
		this.mIsMutable = other.mIsMutable;
		this.mLeft = other.mLeft;
		this.mTop = other.mTop;
		this.mValidator = other.mValidator;
	}

	public static CDrawable createCDrawable(Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return new CDrawable((BitmapDrawable) drawable);
		} else if (drawable instanceof NinePatchDrawable) {
		    Debug.asserts(false);
            return null;
		} else if (drawable instanceof CDrawable) {
			return (CDrawable) drawable;
		} else {
			Debug.asserts(false);
			return null;
		}
	}

	@Override
	public void draw(Canvas c) {
		if (mInvalidate) {
			validate();
		}
		if (mBoundsChange) {
			Log.d("BoundsChange", "bounds change");
			Bitmap newBitmap = Bitmap.createScaledBitmap(mBitmap,
					mDstRect.width(), mDstRect.height(), true);
			if (newBitmap != mBitmap || newBitmap != null) {
				if (mIsMutable) {
					mBitmap.recycle();
				} 
				mBitmap = newBitmap;
				mIsMutable = true;
				Debug.asserts(mBitmap.getWidth() == mDstRect.width()
						&& mBitmap.getHeight() == mDstRect.height());
				mBoundsChange = false;
			}

			c.drawBitmap(mBitmap, null, mDstRect, mPaint);

		} else {
			Log.d("drawb", "bounds change");
			c.drawBitmap(mBitmap, mLeft, mTop, mPaint);
		}
	}

	@Override
	public int getIntrinsicHeight() {
		return mBitmap.getHeight();
	}

	@Override
	public int getIntrinsicWidth() {
		return mBitmap.getWidth();
	}

	@Override
	public int getMinimumHeight() {
		return getIntrinsicHeight();
	}

	@Override
	public int getMinimumWidth() {
		return getIntrinsicWidth();
	}

	@Override
	protected void onBoundsChange(Rect bounds) {
		if (bounds.isEmpty()) {
			return;
		}
		final int left = bounds.left;
		final int right = bounds.right;
		final int top = bounds.top;
		final int bottom = bounds.bottom;

		int width = right - left;
		int height = bottom - top;
		Bitmap bitmap = mBitmap;
		if (bitmap != null) {
			int bWidth = bitmap.getWidth();
			int bHeight = bitmap.getHeight();
			if (width != bWidth || height != bHeight) {
				mBoundsChange = true;
				if (mDstRect == null) {
					mDstRect = new Rect();
				}
				mDstRect.set(bounds);
			}
		}
		mLeft = left;
		mTop = top;
	}

	@Override
	public void setBounds(Rect bounds) {
		super.setBounds(bounds);
	}

	@Override
	public int getOpacity() {
		return 0;
	}

	public void recycle() {
		if (mIsMutable) {
			mBitmap.recycle();
			mBitmap = null;
		}
	}

	public Bitmap getBitmap() {
		return mBitmap;
	}

	@Override
	public void setAlpha(int arg0) {
	}

	@Override
	public void setColorFilter(ColorFilter arg0) {
	}

	public void swap(final CDrawable drawable) {
		final Bitmap t = mBitmap;
		mBitmap = drawable.mBitmap;
		drawable.mBitmap = t;

		final int left = mLeft;
		mLeft = drawable.mLeft;
		drawable.mLeft = left;

		final int top = mTop;
		mTop = drawable.mTop;
		drawable.mTop = top;

		final boolean mutable = mIsMutable;
		mIsMutable = drawable.mIsMutable;
		drawable.mIsMutable = mutable;
	}
}
