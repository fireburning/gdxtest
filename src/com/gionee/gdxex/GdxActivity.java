package com.gionee.gdxex;


import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL11;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ConfigurationInfo;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

public class GdxActivity extends Activity implements Application {
    private AndroidGraphics graphics;
    private ApplicationListener listener;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGLSurfaceView = new GLSurfaceView(this);
        if (detectOpenGLES20()) {
            // Tell the surface view we want to create an OpenGL ES 2.0-compatible
            // context, and set an OpenGL ES 2.0-compatible renderer.
            mGLSurfaceView.setEGLContextClientVersion(2);
            mGLSurfaceView.setRenderer(new GLES20TriangleRenderer(this));
        }
        setContentView(mGLSurfaceView);
    }

    private boolean detectOpenGLES20() {
        ActivityManager am =
            (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ConfigurationInfo info = am.getDeviceConfigurationInfo();
        return (info.reqGlEsVersion >= 0x20000);
    }

    @Override
    protected void onResume() {
        // Ideally a game should implement onResume() and onPause()
        // to take appropriate action when the activity looses focus
        super.onResume();
        mGLSurfaceView.onResume();
    }

    @Override
    protected void onPause() {
        // Ideally a game should implement onResume() and onPause()
        // to take appropriate action when the activity looses focus
        super.onPause();
        mGLSurfaceView.onPause();
    }

    public void initialize (ApplicationListener listener, boolean useGL2IfAvailable) {
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useGL20 = useGL2IfAvailable;
        initialize(listener, config);
    }

    /** This method has to be called in the {@link Activity#onCreate(Bundle)} method. It sets up all the things necessary to get
     * input, render via OpenGL and so on. If config.useGL20 is set the AndroidApplication will try to create an OpenGL ES 2.0
     * context which can then be used via {@link Graphics#getGL20()}. The {@link GL10} and {@link GL11} interfaces should not be
     * used when OpenGL ES 2.0 is enabled. To query whether enabling OpenGL ES 2.0 was successful use the
     * {@link Graphics#isGL20Available()} method. You can configure other aspects of the application with the rest of the fields in
     * the {@link AndroidApplicationConfiguration} instance.
     * 
     * @param listener the {@link ApplicationListener} implementing the program logic
     * @param config the {@link AndroidApplicationConfiguration}, defining various settings of the application (use accelerometer,
     *           etc.). */
    public void initialize (ApplicationListener listener, AndroidApplicationConfiguration config) {
//        graphics = new AndroidGraphics(this, config, config.resolutionStrategy == null ? new FillResolutionStrategy()
//        : config.resolutionStrategy);
        this.listener = listener;
        this.handler = new Handler();

        Gdx.app = this;
        Gdx.graphics = this.getGraphics();
        Debug.asserts(Gdx.graphics != null);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        } catch (Exception ex) {
            log("AndroidApplication", "Content already displayed, cannot request FEATURE_NO_TITLE", ex);
        }
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        setContentView(graphics.getView());
    }
    
    private GLSurfaceView mGLSurfaceView;

    @Override
    public Graphics getGraphics() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void log(String tag, String message) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void log(String tag, String message, Exception exception) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void error(String tag, String message) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void error(String tag, String message, Throwable exception) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void debug(String tag, String message) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void debug(String tag, String message, Throwable exception) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void setLogLevel(int logLevel) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public ApplicationType getType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int getVersion() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getJavaHeap() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getNativeHeap() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void postRunnable(Runnable runnable) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void exit() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void addLifecycleListener(LifecycleListener listener) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void removeLifecycleListener(LifecycleListener listener) {
        // TODO Auto-generated method stub
        
    }
}
