/*
 * Copyright (C) 2009 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gionee.gdxex;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix3;
import com.badlogic.gdx.math.Matrix4;

class GLES20TriangleRenderer implements GLSurfaceView.Renderer {
    /** sprite batch to draw text **/
    private SpriteBatch spriteBatch;
    /** the background texture **/
    private Texture backgroundTexture;
    /** view and transform matrix for text rendering and transforming 3D objects **/
    private final Matrix4 viewMatrix = new Matrix4();
    private final Matrix4 transform = new Matrix4();
    private final Matrix4 normal = new Matrix4();
    private final Matrix3 normal3 = new Matrix3();
    
    public GLES20TriangleRenderer(Context context) {
        mContext = context;
        spriteBatch = new SpriteBatch();
        backgroundTexture = new Texture(Gdx.files.internal("data/planet.jpg"), Format.RGB565, true);
        backgroundTexture.setFilter(TextureFilter.MipMap, TextureFilter.Linear);
    }

    private void renderBackground () {
        viewMatrix.setToOrtho2D(0, 0, 400, 320);
        spriteBatch.setProjectionMatrix(viewMatrix);
        spriteBatch.begin();
        spriteBatch.disableBlending();
        spriteBatch.setColor(Color.WHITE);
        spriteBatch.draw(backgroundTexture, 0, 0, 480, 320, 0, 0, 512, 512, false, false);
        spriteBatch.end();
    }
    
    public void onDrawFrame(GL10 glUnused) {
        // Ignore the passed-in GL10 interface, and use the GLES20
        // class's static methods instead.

        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);
        renderBackground();
    }

    public void onSurfaceChanged(GL10 glUnused, int width, int height) {
      
    }

    public void onSurfaceCreated(GL10 glUnused, EGLConfig config) {

    }


    private Context mContext;
}
